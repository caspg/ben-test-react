# ActivityLog structure
## Idea:
Main idea is to have to separate containers connected to the redux store: `ActivityLogContainer` and `ActivityListContainer`.
Each one will handle different actions. `ActivityListContainer` will be rendered inside `ActivityLogContainer`.
This would allow to reuse `ActivityListContainer` (and `ActivityList`) in different parts of aplication.

## Basic overview:
* `ActivityLogContainer`
  * Root component conected to the store. It should handle single action: `createLogEntry`.
  * **child components**:
    * `ActivityLogLayout`
      * Component which will define main structure. It would render MediaPlan title and child components.
      * **child components**:
        * `ActivityListContainer`
          * **child components**: `ActivityList`, `LogEntry`, `DeleteButton`
        * `ActivityLogForm`
