const initialState = {
  allIds: [],
  byId: {},
};

const mediaPlansReducer = (state = initialState) => state;

export default mediaPlansReducer;
