import { combineReducers } from 'redux';

import currentUser from './currentUser';
import logEntries from './logEntries';
import mediaPlans from './mediaPlans';

const rootReducer = combineReducers({
  currentUser,
  logEntries,
  mediaPlans,
});

export default rootReducer;
