const initialState = {
  id: null,
  username: '',
};

const currentUserReducer = (state = initialState) => state;

export default currentUserReducer;
