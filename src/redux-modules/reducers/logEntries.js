import { DELETE_LOG_ENTRY } from '../actions/logEntries';

const initialState = {
  allIds: [],
  byId: {},
};

const logEntriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_LOG_ENTRY: {
      const logEntry = state.byId[action.logEntryId];
      const updatedLogEntry = Object.assign({}, logEntry, {
        status: 'deleted',
        message: 'The message was deleted.',
      });
      const newById = Object.assign({}, state.byId, {
        [action.logEntryId]: updatedLogEntry,
      });

      return ({
        allIds: state.allIds.filter(id => id !== action.logEntryId),
        byId: newById,
      });
    }

    default:
      return state;
  }
};

export default logEntriesReducer;
