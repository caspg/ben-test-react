import { createStore } from 'redux';

import rootReducer from './reducers/root';

const store = initialState =>
  createStore(
    rootReducer,
    initialState,
  );

export default store;
