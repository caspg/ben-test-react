const DELETE_LOG_ENTRY = 'DELETE_LOG_ENTRY';

const deleteLogEntry = logEntryId => ({
  type: DELETE_LOG_ENTRY,
  logEntryId,
});

export {
  DELETE_LOG_ENTRY,
  deleteLogEntry,
};
