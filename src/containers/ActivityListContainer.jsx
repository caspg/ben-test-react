import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import ActivityList from '../components/ActivityList';
import { deleteLogEntry } from '../redux-modules/actions/logEntries';

class ActivityListContainer extends Component {
  constructor(props) {
    super(props);

    this.handleDeleteLogEntry = this.handleDeleteLogEntry.bind(this);
  }

  handleDeleteLogEntry(logEntryId) {
    this.props.dispatch(
      deleteLogEntry(logEntryId),
    );
  }

  render() {
    const { currentUser, logEntries } = this.props;

    return (
      <div style={{ margin: 50, maxWidth: 600 }}>
        <ActivityList
          currentUser={currentUser}
          activityLogs={logEntries}
          onDeleteLogEntry={this.handleDeleteLogEntry}
        />
      </div>
    );
  }
}

ActivityListContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
  logEntries: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const getLogEntries = (mediaPlan, logEntries) => {
  if (!mediaPlan) return null;

  return mediaPlan.logEntries.map(logEntryId =>
    logEntries.byId[logEntryId],
  );
};

const mapStateToProps = (state, ownProps) => {
  const { mediaPlanId } = ownProps;
  const { mediaPlans, logEntries } = state;
  const mediaPlan = mediaPlans.byId[mediaPlanId];
  const logEntriesEntities = getLogEntries(mediaPlan, logEntries);

  return ({
    currentUser: state.currentUser,
    logEntries: logEntriesEntities,
  });
};

export default connect(mapStateToProps)(ActivityListContainer);
