export default [
  {
    "id": "1",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475661720,
    "action": "create",
    "message": "The MediaPlan was created."
  },
  {
    "id": "2",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475755320,
    "action": "update",
    "message": "The budget was changed."
  },
  {
    "id": "3",
    "mediaPlanId": "123456789",
    "userId": "2",
    "username": "Joe",
    "date": 1475773320,
    "action": "update",
    "message": "The budget was changed."
  },
  {
    "id": "4",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475775000,
    "action": "message",
    "message": "Joe, why did you change the budget?"
  },
  {
    "id": "5",
    "mediaPlanId": "123456789",
    "userId": "2",
    "username": "Joe",
    "date": 1475832600,
    "action": "message",
    "message": "The message was deleted.",
    "status": "deleted"
  },
  {
    "id": "6",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475836200,
    "action": "message",
    "message": "That's not right and it's also private information, please remove it. I will change the budget to the original values."
  },
  {
    "id": "7",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475836320,
    "action": "update",
    "message": "The budget was changed."
  }
]
