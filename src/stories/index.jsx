import React from 'react';
import { storiesOf, action } from '@kadira/storybook';

import activityLogs from '../data';
import ActivityList from '../components/ActivityList';

import ActivityListApp from './ActivityListApp';

const currentUser = {
  id: '1',
  username: 'Andu',
};

storiesOf('ActivityList', module)
  .add('default view', () => (
    <div style={{ margin: 50, maxWidth: 600 }}>
      <ActivityList
        activityLogs={activityLogs}
        currentUser={currentUser}
        onDeleteLogEntry={(logEntryId) => { action(`onDeleteLogEntry clicked with logEntryId: ${logEntryId}`)(); }}
      />
    </div>
   ))
   .add('connected to the redux store', () => (
     <ActivityListApp />
   ));
