import logEntriesData from '../../data';

const logEntriesIds = logEntriesData.map(d => d.id);
const logEntriesById = logEntriesData.reduce((obj, current) => {
  obj[current.id] = current; // eslint-disable-line no-param-reassign
  return obj;
}, {});

const initialState = {
  currentUser: {
    id: '1',
    username: 'Andu',
  },
  mediaPlans: {
    allIds: ['123456789'],
    byId: {
      123456789: {
        id: '123456789',
        logEntries: ['1', '2', '3', '4', '5', '6', '7'],
      },
    },
  },
  logEntries: {
    allIds: logEntriesIds,
    byId: logEntriesById,
  },
};

export default initialState;
