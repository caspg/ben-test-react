import React from 'react';
import { Provider } from 'react-redux';
import ActivityListContainer from '../../containers/ActivityListContainer';

import createStore from '../../redux-modules/store';
import initialState from './initialState';

// in the really app this could come from url param
const mediaPlanId = '123456789';

const store = createStore(initialState);

const ActivityListApp = () => (
  <Provider store={store}>
    <ActivityListContainer mediaPlanId={mediaPlanId} />
  </Provider>
);

export default ActivityListApp;
