import React, { PropTypes } from 'react';

import './ActivityList.css';
import LogEntry from './components/LogEntry';

const ActivityList = ({ activityLogs, currentUser, onDeleteLogEntry }) => {
  const renderActivityLog = data => (
    <LogEntry
      key={data.id}
      logEntryData={data}
      currentUser={currentUser}
      onDeleteLogEntry={onDeleteLogEntry}
    />
  );

  return (
    <div className="ActivityList">
      {activityLogs.map(renderActivityLog)}
    </div>
  );
};

ActivityList.defaultProps = {
  activityLogs: [],
};

ActivityList.propTypes = {
  onDeleteLogEntry: PropTypes.func.isRequired,
  activityLogs: PropTypes.arrayOf(Object),
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
};

export default ActivityList;
