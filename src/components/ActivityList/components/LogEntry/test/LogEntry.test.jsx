import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import LogEntry from '../LogEntry';

test('LogEntry renders correctly', () => {
  const logEntryData = {
    message: 'message',
    action: 'create',
    username: 'username',
    date: 1475661720,
    userId: '1',
    id: '1',
    mediaPlanId: '123',
  };
  const currentUser = {
    id: '1',
    username: 'username',
  };

  const shallowComponent = shallow(
    <LogEntry
      logEntryData={logEntryData}
      currentUser={currentUser}
      onDeleteLogEntry={() => {}}
    />,
  );

  expect(toJson(shallowComponent)).toMatchSnapshot();
});
