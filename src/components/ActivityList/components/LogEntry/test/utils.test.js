import { formatDate, formatHeaderText } from '../utils';

test('formatDate', () => {
  const dateNumber = new Date(0).getTime();
  const formatedDate = formatDate(dateNumber);

  expect(
    /00:00, 01.01.1970/.test(formatedDate),
  ).toBe(true);
});

describe('formatHeaderText', () => {
  const username = 'Username';

  const testCases = [
    { action: 'create', isCurrentUser: true, expected: 'You have created the media plan.' },
    { action: 'create', isCurrentUser: false, expected: 'Username has created the media plan.' },
    { action: 'update', isCurrentUser: true, expected: 'You have updated the media plan.' },
    { action: 'update', isCurrentUser: false, expected: 'Username has updated the media plan.' },
    { action: 'message', isCurrentUser: true, expected: 'You said:' },
    { action: 'message', isCurrentUser: false, expected: 'Username said:' },
  ];

  function testFormatHeaderText({ action, isCurrentUser, expected }) {
    test(`${action} action when isCurrentUser equals ${isCurrentUser}`, () => {
      const result = formatHeaderText(action, username, isCurrentUser);
      expect(result).toBe(expected);
    });
  }

  testCases.forEach(testCase => testFormatHeaderText(testCase));
});
