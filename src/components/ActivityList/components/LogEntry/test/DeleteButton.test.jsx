import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import DeleteButton from '../DeleteButton';

describe('DeleteButton', () => {
  it('returns null when logEntryStatus is deleted', () => {
    const wrapper = shallow(
      <DeleteButton
        isCurrentUser={true}
        logEntryStatus="deleted"
        onDeleteLogEntry={() => {}}
        logEntryId="123"
        logEntryAction="message"
      />,
    );

    expect(wrapper.type()).toBe(null);
  });

  it('return null when isCurrentUser is false', () => {
    const wrapper = shallow(
      <DeleteButton
        isCurrentUser={false}
        onDeleteLogEntry={() => {}}
        logEntryId="123"
        logEntryAction="message"
      />,
    );

    expect(wrapper.type()).toBe(null);
  });

  it('returns null when logEntryAction is "message"', () => {
    const wrapper = shallow(
      <DeleteButton
        isCurrentUser={true}
        onDeleteLogEntry={() => {}}
        logEntryId="123"
        logEntryAction="update"
      />,
    );

    expect(wrapper.type()).toBe(null);
  })

  it('renders correctly', () => {
    const wrapper = shallow(
      <DeleteButton
        isCurrentUser={true}
        onDeleteLogEntry={() => {}}
        logEntryId="123"
        logEntryAction="message"
      />,
    );

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
