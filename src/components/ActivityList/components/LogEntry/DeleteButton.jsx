import React, { PropTypes } from 'react';

const isStatusDeleted = status => status === 'deleted';
const isActionTypeMessage = action => action === 'message';

const DeleteButton = (props) => {
  if (!isActionTypeMessage(props.logEntryAction)) return null;

  if ((!props.isCurrentUser || isStatusDeleted(props.logEntryStatus))) {
    return null;
  }

  const handleOnClick = () =>
    props.onDeleteLogEntry(props.logEntryId);

  return (
    <button onClick={handleOnClick}>
      X
    </button>
  );
};

DeleteButton.defaultProps = {
  logEntryStatus: '',
};

DeleteButton.propTypes = {
  isCurrentUser: PropTypes.bool.isRequired,
  logEntryStatus: PropTypes.string,
  onDeleteLogEntry: PropTypes.func.isRequired,
  logEntryId: PropTypes.string.isRequired,
  logEntryAction: PropTypes.string.isRequired,
};

export default DeleteButton;
