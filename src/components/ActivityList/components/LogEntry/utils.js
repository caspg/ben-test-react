import dateFormat from 'date-fns/format';

const messageBasedOnAction = {
  create: (username, verb) => `${username} ${verb} created the media plan.`,
  update: (username, verb) => `${username} ${verb} updated the media plan.`,
  message: username => `${username} said:`,
};

function formatHeaderText(action, username, isCurrentUser) {
  const verb = isCurrentUser ? 'have' : 'has';
  const usernameDisplayText = isCurrentUser ? 'You' : username;

  return messageBasedOnAction[action](usernameDisplayText, verb);
}

function formatDate(date) {
  return dateFormat(date, 'HH:mm, DD.MM.YYYY');
}

export {
  formatDate,
  formatHeaderText,
};
