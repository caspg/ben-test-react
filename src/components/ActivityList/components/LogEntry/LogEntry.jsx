import React, { PropTypes } from 'react';

import './LogEntry.css';
import { formatDate, formatHeaderText } from './utils';
import DeleteButton from './DeleteButton';

const LogEntry = ({ logEntryData, currentUser, onDeleteLogEntry }) => {
  const { username, action } = logEntryData;
  const isCurrentUser = currentUser.id === logEntryData.userId;
  const headerText = formatHeaderText(action, username, isCurrentUser);

  return (
    <div className="LogEntry" style={{ borderColor: isCurrentUser ? '#444' : '#ddd' }}>
      <div className="container">
        <div className="row">
          <div className="ten columns">
            <h5 className="LogEntry-header-text">
              {headerText}
            </h5>
          </div>

          <div className="two columns">
            <DeleteButton
              isCurrentUser={isCurrentUser}
              onDeleteLogEntry={onDeleteLogEntry}
              logEntryId={logEntryData.id}
              logEntryStatus={logEntryData.status}
              logEntryAction={logEntryData.action}
            />
          </div>
        </div>

        <div className="row">
          <div className="column">
            <p className="LogEntry-message">
              {logEntryData.message}
            </p>
          </div>
        </div>

        <div className="row">
          <div className="column">
            <span className="u-pull-right LogEntry-date">
              on {formatDate(logEntryData.date)}
            </span>
          </div>
        </div>

      </div>
    </div>
  );
};

LogEntry.propTypes = {
  onDeleteLogEntry: PropTypes.func.isRequired,
  logEntryData: PropTypes.shape({
    message: PropTypes.string.isRequired,
    action: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    date: PropTypes.number.isRequired,
    userId: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    mediaPlanId: PropTypes.string.isRequired,
  }).isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
};

export default LogEntry;
