import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import ActivityList from '../ActivityList';

test('ActivityList renders correctly', () => {
  const currentUser = {
    id: '1',
    username: 'username',
  };
  const activityLogs = [
    {
      id: '1',
      mediaPlanId: '123456789',
      userId: '1',
      username: 'Andu',
      date: 1475661720,
      action: 'create',
      message: 'The MediaPlan was created.',
    },
    {
      id: '2',
      mediaPlanId: '123456789',
      userId: '1',
      username: 'Andu',
      date: 1475755320,
      action: 'update',
      message: 'The budget was changed.',
    },
  ];

  const shallowComponent = shallow(
    <ActivityList
      currentUser={currentUser}
      activityLogs={activityLogs}
      onDeleteLogEntry={() => {}}
    />,
  );

  expect(toJson(shallowComponent)).toMatchSnapshot();
});
