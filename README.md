# Ben Coding Test - Client

This is a practical code test for the interview process at BEN. It's a React application (created with [create-react-app](https://github.com/facebookincubator/create-react-app)) with [Storybook](https://github.com/storybooks/react-storybook) support.

React Storybook is a development environment for React components that allows you to develop them interactively and view their different states without the need of having the actual application developed.

Feel free to use something else if you don't like React Storybook or if you have your own React boilerplate that you prefer. We've set up Storybook for you to make it easier to start but it's not mandatory to use our setup.

## Setup

You're going to need NodeJS.

* `npm run storybook` starts a storybook server on port 9009: [http://localhost:9009/](http://localhost:9009/).

Navigating to Storybook you'll see it contains three components: Welcome and Button components (the default examples from Storybook) and an empty one called ActivityList, with one state, `main view`. The code for the component is in `src/stories/ActivityList.js`.

## Tasks prerequisites

Let's assume that our web application works with an entity called `MediaPlan`. It's the object that holds the budget a client has for product placements, the movies and TV shows it wants to advertise on and other various information. A `MediaPlan`, usually, is going to be edited by more than one person, for example one of our sales people and also someone from the client's side.

Because of this we have an activity log which is a list of all the operations that were performed on a `MediaPlan` so that we can see who changed what and when. When somebody edits the object, we create a record in the log with the current user, the date, the operation (add, create, delete), and a message that describes what exactly has changed (i.e. "Changed title and description.").

For example:

```
  {
    "id": "1",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475661720,
    "action": "create",
    "message": "The MediaPlan was created."
  }
```

We also have a chat system that is integrated into the log, where each message also creates a record in the log with the user that sent the message, the date and the actual message.

For example:

```
  {
    "id": "3",
    "mediaPlanId": "123456789",
    "userId": "1",
    "username": "Andu",
    "date": 1475775000,
    "action": "message",
    "message": "Joe, why did you change the budget?"
  }
```

Because a client or someone from sales can accidentally send a message with private information, we also allow the users to delete their own message. When that happens, we don't completely remove the entry from the log as, if it would be part of a chat with someone else, it could look weird afterwards. What we do is set the message contents to "This message was deleted" and mark the record in the log as such.

A mockup of an activity log is provided in the `data.json` file in this repository. There are two users in the log: Andu and Joe. Andu first creates a `MediaPlan` and then changes the budget. Joe then changes the budget as well and Andu asks why he did that. Joe replies with some information that shouldn't be public so Andu asks him to remove it and changes the budget to the original values. Joe then deletes his message.

To show this in the site we use two components: `ActivityLog` and `ActivityList`.

The `ActivityLog` is a component that shows all the entries in the log in a list and also provides an input box so that the current user can add another message.

The list contains all the records in the log, and based on what type of action they are (message or create/update/delete operation) look different. Messages from the current user, who haven't been deleted, also have a delete action enabling the user to do that.

There are two wireframes provided as a guideline: `wireframe-ActivityLog.png` (how the component looks) and `wireframe-LogEntry.png` (how the various messages look like). Another example of how the component looks is the chat inside the Facebook website.

The `ActivityList` component is very similar to the `ActivityLog` one, with the exception that it does not allow the entering of new messages. It could be used, for example, on a page that shows, for a particular `MediaPlan`, the activity log in a full page list with pagination. Users would still be able to delete their own messages, just like in the `ActivityLog`.

The way it looks is provided in the `wireframe-ActivityList.png`.

## Tasks

* Tell us how you would go about structuring the `ActivityLog` component. What sub-components would you create etc?
* Implement the ActivityList component using the mock data provided and assuming the current user who views the component is Andu.
* Implement a redux store that the component will use that will allow the current user to delete his messages.

## Notes

* It's not required to write any server side code to persist the log in a database, use the supplied mock data in an array and mutate that.
* Do not spend time making it pretty.
* Feel free to use any other build system / boilerplate / code structure / etc that you're comfortable with.
* The purpose of this test is to see how comfortable you are with React and Redux. Don't spend more than two hours on this (as we're not paying you for this we don't want you to waste too much time on it), if you're not done with it then send us what you have so far, non-working code is fine.
* We'd appreciate it if you would let us know your impression on the test. Was it too easy? Too difficult? What would you add or remove from it?


## Resources
* [React Storybook](https://getstorybook.io).
* [Writing Stories](https://getstorybook.io/docs/react-storybook/basics/writing-stories) for React Storybook.
* [Using Redux with React](http://redux.js.org/docs/basics/UsageWithReact.html)
